package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type Cake struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Oven      bool   `json:"oven"`
	Flour     bool   `json:"flour"`
	Shop      bool   `json:"shop"`
}

type CakeContract struct {
	contractapi.Contract
}

func (c *CakeContract) CreateCake(ctx contractapi.TransactionContextInterface, id string, name string) error {
	// Check if a cake record with the given id already exists
	existingCakeJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return err
	}
	if existingCakeJSON != nil {
		return fmt.Errorf("a cake record with the id '%s' already exists", id)
	}
	cake := Cake{
		ID:   id,
		Name: name,
	}
	cakeJSON, err := json.Marshal(cake)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(id, cakeJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Created cake: %s", id)
	err = ctx.GetStub().SetEvent("CreateCake", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

func (c *CakeContract) ReadCake(ctx contractapi.TransactionContextInterface, id string) (*Cake, error) {
	cakeJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if cakeJSON == nil {
		return nil, fmt.Errorf("the cake %s does not exist", id)
	}
	var cake Cake
	err = json.Unmarshal(cakeJSON, &cake)
	if err != nil {
		return nil, err
	}
	eventPayload := fmt.Sprintf("Read cake: %s", id)
	err = ctx.GetStub().SetEvent("ReadCake", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register. %v", err)
	}
	return &cake, nil
}

func (c *CakeContract) UpdateCake(ctx contractapi.TransactionContextInterface, cake Cake) error {
	cakeJSON, err := json.Marshal(cake)
	if err != nil {
		return err
	}
	err = ctx.GetStub().PutState(cake.ID, cakeJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}
	eventPayload := fmt.Sprintf("Updated cake: %s", cake.ID)
	err = ctx.GetStub().SetEvent("UpdateCake", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}
	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(CakeContract))
	if err != nil {
		fmt.Printf("Error creating cake chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting cake chaincode: %s", err.Error())
	}
}
